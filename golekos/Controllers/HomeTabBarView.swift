//
//  HomeTabBarViewController.swift
//  golekos
//
//  Created by AkbarPuteraW on 25/02/21.
//  Copyright © 2021 FiveMacbook. All rights reserved.
//

import UIKit

class HomeTabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHome()
        setWhistlist()
        UITabBar.appearance().tintColor = ColorAsset.tabbarTint
        
    }
    
    
    fileprivate func setHome() {
        if let homeItem = self.tabBar.items?[0] {
            homeItem.title(.home)
        }
    }
    
    fileprivate func setWhistlist() {
        if let homeItem = self.tabBar.items?[1] {
            homeItem.title(.whistlist)
        }
    }
    
}

extension UITabBarItem {
    func title(_ homeTab: BarCustom){
        self.title = homeTab.description
        self.imageInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        self.image = homeTab.reImage.withRenderingMode(.alwaysOriginal)
        self.image = homeTab.reImage
    }
}
