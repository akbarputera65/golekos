//
//  SecondViewController.swift
//  golekos
//
//  Created by AkbarPuteraW on 25/02/21.
//  Copyright © 2021 FiveMacbook. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var bookingView: UIView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var specsCollectionView: UICollectionView!
    @IBOutlet weak var happyTenantTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        registerCellView()
        setFlowLayout()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    fileprivate func initView() {
        self.bookingView.backgroundColor = ColorAsset.bookingView
        self.detailView.roundCorners(corners: [.topLeft,.topRight], radius: 30)
        self.detailView.backgroundColor = ColorAsset.mainBackground
    }
    
    fileprivate func registerCellView() {
        let caroselNib = UINib(nibName: "CaroselCollectionViewCell", bundle: nil)
        let specsNib = UINib(nibName: "SpecsCollectionViewCell", bundle: nil)
        let tenanTableNib = UINib(nibName: "HappyTableViewCell", bundle: nil)
        collectionView.register(caroselNib, forCellWithReuseIdentifier: "collectionCell")
        specsCollectionView.register(specsNib, forCellWithReuseIdentifier: "specsCell")
        happyTenantTableView.register(tenanTableNib, forCellReuseIdentifier: "tableViewCell")
    }
    
    fileprivate func setFlowLayout() {
        let size = self.collectionView.frame.size
        let flow = UICollectionViewFlowLayout()
        flow.itemSize = CGSize(width: size.width, height: size.height)
        flow.scrollDirection = .horizontal
        flow.minimumLineSpacing = 0
        collectionView.setCollectionViewLayout(flow, animated: true)
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SecondViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.collectionView:
            return 4
        case self.specsCollectionView:
            return RoomSpecs.setData().count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.collectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath)
            return cell
        case self.specsCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "specsCell", for: indexPath) as! SpecsCollectionViewCell
            let roomData = RoomSpecs.setData()[indexPath.row]
            cell.configure(model: roomData)
            return cell
        default:
            return UICollectionViewCell()
        }
        
    }
}

extension SecondViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as! HappyTableViewCell
        
        return cell
    }
}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
