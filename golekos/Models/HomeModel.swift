//
//  HomeModel.swift
//  golekos
//
//  Created by AkbarPuteraW on 26/02/21.
//  Copyright © 2021 FiveMacbook. All rights reserved.
//

import Foundation

struct HomeModel {
    
    var image: String
    var name: String
    var status: String
    var price: String
    
}

extension HomeModel {
    static func setData() -> [HomeModel]{
        return [
            HomeModel(image: "pictureOne", name: "Fukko Cozy", status: "Wanita", price: "55"),
            HomeModel(image: "pictureTwo", name: "Blue Fast", status: "Umum", price: "21"),
            HomeModel(image: "pictureThree", name: "Jamaixa IIX", status: "Pria", price: "49"),
            HomeModel(image: "pictureOne", name: "Fukko Cozy", status: "Wanita", price: "55"),
            HomeModel(image: "pictureFour", name: "Yaka Past", status: "Wanita", price: "22")
        ]
    }
}
