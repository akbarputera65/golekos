//
//  BarCustom.swift
//  golekos
//
//  Created by AkbarPuteraW on 26/02/21.
//  Copyright © 2021 FiveMacbook. All rights reserved.
//

import UIKit

enum BarCustom {
    
    case home
    case whistlist
    case setting
    case country
    case profile
    
    var reImage: UIImage {
        switch self {
        case .home:
            return #imageLiteral(resourceName: "Icon_home_solid")
        case .whistlist:
            return #imageLiteral(resourceName: "Icon_love_solid")
        case .setting:
            return #imageLiteral(resourceName: "Icon_filter_solid")
        case .country:
            return #imageLiteral(resourceName: "Icon_language_solid")
        case .profile:
            return #imageLiteral(resourceName: "Icon_user_solid")
        }
    }
    
    var description: String {
        switch self {
        case .home:
            return "Home"
        case .whistlist:
            return "Whislit"
        case .setting:
            return "Setting"
        case .country:
            return "Country"
        case .profile:
            return "Profile"
        }
    }
}
