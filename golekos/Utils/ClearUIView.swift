//
//  ClearUIView.swift
//  golekos
//
//  Created by FiveMacbook on 25/02/21.
//  Copyright © 2021 FiveMacbook. All rights reserved.
//

import UIKit

final class ClearUIView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.backgroundColor = .clear
    }
}

final class RoundedUIView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = 10
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.layer.cornerRadius = 10
    }
}

