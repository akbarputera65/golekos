//
//  ColorAssets.swift
//  golekos
//
//  Created by FiveMacbook on 25/02/21.
//  Copyright © 2021 FiveMacbook. All rights reserved.
//

import UIKit

final class ColorAsset {
    
    static let mainBackground = UIColor.rgb(r: 242, g: 246, b: 253)
    static let tabbarTint = UIColor.rgb(r: 45, g: 198, b: 242)
    static let bookingView = UIColor.rgb(r: 41, g: 213, b: 248)
}

extension UIColor {
    static func rgb(r:CGFloat, g:CGFloat, b:CGFloat) -> UIColor {
        return UIColor(red: r/255, green: b/255, blue: b/255, alpha: 1)
    }
}
