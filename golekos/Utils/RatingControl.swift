//
//  RatingControl.swift
//  golekos
//
//  Created by AkbarPuteraW on 26/02/21.
//  Copyright © 2021 FiveMacbook. All rights reserved.
//

import UIKit

@IBDesignable class RatingControl: UIStackView {
    
    //MARK: Properties
    private var ratingButtons = [UIButton]()
    
    var rating = 0 {
        didSet {
            self.updateButtonSelectionState()
        }
    }
    
    @IBInspectable var starSize: CGSize = CGSize(width: 0, height: 0) {
        didSet{
            self.setupButton()
        }
    }
    
    @IBInspectable var starCount: Int = 0 {
        didSet{
            self.setupButton()
        }
    }
    
    var buttonStar: UIButton = {
        let button = UIButton()
        button.backgroundColor = .red
        
        
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupButton()
    }
    
    private func setupButton() {
        clearButton()
        
        let bundle = Bundle(for: type(of: self))
        let fillStar = UIImage(named: "Icon_star_solid", in: bundle, compatibleWith: self.traitCollection)
        let emptyStar = UIImage(named: "Vector 388", in: bundle, compatibleWith: self.traitCollection)
        
        for _ in 0..<starCount {
            let button = UIButton()
            button.setImage(emptyStar, for: .normal)
            button.setImage(fillStar, for: .selected)
            button.setImage(fillStar, for: [.highlighted,.selected])
            button.translatesAutoresizingMaskIntoConstraints = false
            button.heightAnchor.constraint(equalToConstant: starSize.height).isActive = true
            button.widthAnchor.constraint(equalToConstant: starSize.width).isActive = true
            button.addTarget(self, action: #selector(ratingButtonAction(button:)), for: .touchUpInside)
            addArrangedSubview(button)
            ratingButtons.append(button)
        }
    }
    
    fileprivate func clearButton () {
        for button in ratingButtons {
            removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        ratingButtons.removeAll()
    }
    
    fileprivate func updateButtonSelectionState() {
        for (index,button) in ratingButtons.enumerated() {
            button.isSelected = index < rating
        }
    }
    //MARK: Button Action
    @objc func ratingButtonAction(button: UIButton) {
        guard let index = ratingButtons.firstIndex(of: button) else {
            fatalError("The button, \(button), is not in the ratingButtons array: \(ratingButtons)")
        }
        
        let selectedRating = index + 1
        
        if selectedRating == rating {
            rating = 0
        }else{
            rating = selectedRating
        }
        
    }
    
}
