//
//  HappyTableViewCell.swift
//  golekos
//
//  Created by AkbarPuteraW on 27/02/21.
//  Copyright © 2021 FiveMacbook. All rights reserved.
//

import UIKit

class HappyTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.backgroundColor = .clear
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
