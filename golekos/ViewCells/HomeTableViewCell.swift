//
//  HomeTableViewCell.swift
//  golekos
//
//  Created by AkbarPuteraW on 26/02/21.
//  Copyright © 2021 FiveMacbook. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureDataCell(model data: HomeModel) {
        self.picture.image = UIImage(named: "\(data.image)")
        self.nameLabel.text  = data.name
        self.statusLabel.text  = data.status
        self.priceLabel.text = "$ \(data.price)"
    }
}
