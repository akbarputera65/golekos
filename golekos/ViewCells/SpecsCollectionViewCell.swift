//
//  SpecsCollectionViewCell.swift
//  golekos
//
//  Created by AkbarPuteraW on 26/02/21.
//  Copyright © 2021 FiveMacbook. All rights reserved.
//

import UIKit

struct RoomSpecs {
    
    var icon: String
    var title: String
    var rating: String
    
    static func setData() -> [RoomSpecs] {
        return [
            RoomSpecs(icon: "002-double-bed", title: "Master Bad", rating: "3"),
            RoomSpecs(icon: "001-bath", title: "Bathroom", rating: "2"),
            RoomSpecs(icon: "003-bar-counter", title: "Kitchen", rating: "1")
        ]
    }
}

class SpecsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var iconVIew: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configure(model room: RoomSpecs) {
        iconVIew.image = UIImage(named: room.icon)
        titleLabel.text = room.title
        ratingLabel.text = room.rating
    }

}
